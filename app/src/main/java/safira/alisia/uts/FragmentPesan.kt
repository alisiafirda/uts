package safira.alisia.uts

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_pesan.*
import kotlinx.android.synthetic.main.frag_pesan.view.*


@Suppress("DEPRECATION")
class FragmentPesan : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0, true)
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnHapus ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }
            R.id.btnTambah ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }

            R.id.btnCari ->{
                showDataMhs(edMenu.text.toString())
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    var namaPesan : String = ""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_pesan,container,false)
        db = thisParent.getDBObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnHapus.setOnClickListener(this)
        v.btnTambah.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.btnCari.setOnClickListener(this)

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataPesan("")
    }

    fun showDataMhs(namaMhs : String){
        var sql = ""
        if(!namaMhs.trim().equals("")){
            sql = "select m.nim as _id, m.nama, p.nama_prodi from mhs m, prodi p " +
                    "where m.id_prodi = p.id_prodi and m.nama like '%$namaMhs%'"
        }else{
            sql = "select m.nim as _id, m.nama, p.nama_prodi from mhs m, prodi p " +
                    "where m.id_prodi = p.id_prodi order by m.nama asc"
        }
        val c : Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_pesan,c,
            arrayOf("_id","nama","nama_prodi"), intArrayOf(R.id.txHarga, R.id.txMenu),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsPesan.adapter = lsAdapter
    }



    fun insertDataPesan(kode_pesan : String, nama_pesan: String){
        var sql = "insert into pesan ( nama, harga) values (?,?)"
        db.execSQL(sql, arrayOf(kode_pesan, nama_pesan))
        showDataPesan("")
    }



}