package safira.alisia.uts

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context): SQLiteOpenHelper(context,DB_Name, null, DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tPesan = "create table pesan(kode_pesan text primary key, nama_pesan text not null, jml_beli int not null, harga int not null)"
        val tMenu = "create table menu(kode_menu integer primary key , nama_menu text not null, detail_menu text not null, harga int not null)"

        db?.execSQL(tPesan)
        db?.execSQL(tMenu)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "pemesanan"
        val DB_Ver = 1
    }
}